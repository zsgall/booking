package com.housebooking.vo;

public enum Role {
	MANAGER,
	CLIENT,
	CLEANER
}
