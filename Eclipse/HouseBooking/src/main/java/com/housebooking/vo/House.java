package com.housebooking.vo;

import io.swagger.v3.oas.annotations.media.Schema;

public class House {

	@Schema
	private int capacity;
	@Schema
	private String address;
	@Schema
	private boolean cleaned;
	@Schema
	private boolean prepared;
	@Schema
	private boolean reserved;

	public House() {
		
	}
	
	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean getCleaned() {
		return cleaned;
	}

	public void setCleaned(boolean cleaned) {
		this.cleaned = cleaned;
	}

	public boolean getPrepared() {
		return prepared;
	}

	public void setPrepared(boolean prepared) {
		this.prepared = prepared;
	}

	public boolean getReserved() {
		return reserved;
	}

	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}

}
