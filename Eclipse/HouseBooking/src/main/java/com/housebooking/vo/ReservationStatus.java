package com.housebooking.vo;

public enum ReservationStatus {
	CONFIRMED,
	DENIED,
	PENDING
}
