package com.housebooking.vo;

import com.housebooking.service.BookingService;

import io.swagger.v3.oas.annotations.media.Schema;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Reservation {
	
	@Schema(description = "Reservation ID")
	private int id;
	@Schema(description = "Reservation start date")
	private String startDate;
	@Schema(description = "Reservation end date")
	private String endDate;
	@Schema(description = "Client user")
	private User client;
	@Schema(description = "Number of persons")
	private int numberOfPersons;
	@Schema(description = "Request date&time")
	private String requestDate;
	@Schema(description = "Reservation status")
	private ReservationStatus status;
	@Schema(description = "Manager the reservation request is responsed by")
	private User responsedBy;
	@Schema(description = "Response date&time")
	private String responseDate;
	@Schema(description = "Final cleaning")
	private boolean finalCleaning;
	@Schema(description = "Rating")
	private Integer rating;
	@Schema(description = "Feedback")
	private String feedback;
	@Schema(description = "Check-in date&time")
	private String checkInDate;
	@Schema(description = "Check-out date&time")
	private String checkOutDate;

	public Reservation() {
		
	}

	public int getId() {
		return id;
	}  
	
	public void setId(int id) {
		this.id = id;
	}   
	
	public String getStartDate() {
		return startDate;
	}

	public Date getSqlStartDate() throws ParseException {
		return startDate == null ? null : new Date(new SimpleDateFormat(BookingService.datePattern).parse(startDate).getTime());
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	} 

	public void setSqlStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new SimpleDateFormat(BookingService.datePattern).format(startDate);
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public Date getSqlEndDate() throws ParseException {
		return endDate == null ? null : new Date(new SimpleDateFormat(BookingService.datePattern).parse(endDate).getTime());
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setSqlEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new SimpleDateFormat(BookingService.datePattern).format(endDate);
	}
		
	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	public int getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(int numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public void setSqlRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate == null ? null : new SimpleDateFormat(BookingService.dateTimePattern).format(requestDate);
	}

	public ReservationStatus getStatus() {
		return status;
	}

	public void setStatus(ReservationStatus status) {
		this.status = status;
	}

	public User getResponsedBy() {
		return responsedBy;
	}

	public void setResponsedBy(User responsedBy) {
		this.responsedBy = responsedBy;
	}

	public String getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}

	public void setSqlResponseDate(Timestamp responseDate) {
		this.responseDate = responseDate == null ? null : new SimpleDateFormat(BookingService.dateTimePattern).format(responseDate);
	}

	public boolean getFinalCleaning() {
		return finalCleaning;
	}

	public void setFinalCleaning(boolean finalCleaning) {
		this.finalCleaning = finalCleaning;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public void setSqlCheckInDate(Timestamp checkInDate) {
		this.checkInDate = checkInDate == null ? null : new SimpleDateFormat(BookingService.dateTimePattern).format(checkInDate);
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public void setSqlCheckOutDate(Timestamp checkOutDate) {
		this.checkOutDate = checkOutDate == null ? null : new SimpleDateFormat(BookingService.dateTimePattern).format(checkOutDate);
	}

}
