package com.housebooking.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class User {

	@Schema(description = "Login name")
	private String loginName;
	@Schema(description = "Password")
	private String password;
	@Schema(description = "Full name")
	private String fullName;
	@Schema(description = "E-mail")
	private String email;
	@Schema(description = "Phone number")
	private String phoneNumber;
	@Schema(description = "Indicates if the user is active or not")
	private boolean active;
	@Schema(description = "User roles list")
	private List<Role> roles;
	
	public User() {
		roles = new ArrayList<>();
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<Role> getRoles() {
		return Collections.unmodifiableList(roles);
	}

	public void addRole(Role role) {
		roles.add(role);
	}

	public void removeRole(Role role) {
		roles.remove(role);
	} 
	
}
