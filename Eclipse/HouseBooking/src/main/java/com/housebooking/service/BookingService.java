package com.housebooking.service;

import com.housebooking.dao.HouseDao;
import com.housebooking.dao.ReservationDao;
import com.housebooking.dao.UserDao;
import com.housebooking.vo.House;
import com.housebooking.vo.Reservation;
import com.housebooking.vo.Role;
import com.housebooking.vo.User;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import org.apache.commons.lang3.StringUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

@OpenAPIDefinition(info = @Info(title = "House booking application", version = "1.0", description = "House booking application"))

@Path("/")
public class BookingService {

	public static final String datePattern = "yyyy-MM-dd";
	public static final String dateTimePattern = "yyyy-MM-dd HH:mm:ss";
	private final String seed = "Jlek32jKLJa4dnO7KHJGH";

	private HouseDao houseDao = new HouseDao();
	private UserDao userDao = new UserDao();
	private ReservationDao reservationDao = new ReservationDao();
	
	private static DataSource dataSource;
	
	static {
		String dataSourceName = "jdbc/HOUSE_BOOKING_DB";
		try {
	        Context initialContext = new InitialContext();
	        Context envContext = (Context)initialContext.lookup("java:/comp/env");        
	        dataSource = (DataSource)envContext.lookup(dataSourceName);			
		}
		catch (NamingException exception) {
            System.out.println("Error while initializing the data source: " + exception.getMessage());
            exception.printStackTrace();
		}
	}
	    
	@POST
	@Path("/prepare")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Prepare house",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response prepareHouse(
			@Parameter(description = "Cleaner login name", required = true)
			@QueryParam("cleaner")
			String cleaner) throws SQLException {

        if (StringUtils.isBlank(cleaner)) {
            return Response.serverError().entity("Parameter 'cleaner' must be provided!").build();
        }

		User user = userDao.getUser(dataSource, cleaner);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + cleaner + ") not found or not active!").build();
		}
		if (!user.getRoles().contains(Role.CLEANER)) {
			return Response.serverError().entity("The given user does not have CLEANER role!").build();
		}
		
		houseDao.setCleaned(dataSource, true);
		houseDao.setPrepared(dataSource, true);
		return Response.ok().build();
	}
	
	@GET
	@Path("/calendar")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Get calendar",
		    responses = {
		    		@ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = Reservation.class))), responseCode = "200")
		    }
	)	
	public Response getCalendar (
			@Parameter(description = "User login name", required = true)
			@QueryParam("user")
			String user,
            @Parameter(description = "Start date", required = true)
            @QueryParam("startDate")
			String startDate,
            @Parameter(description = "End date", required = true)
            @QueryParam("endDate")
			String endDate) throws ParseException, SQLException {

        if (StringUtils.isAnyBlank(user, startDate, endDate)) {
            return Response.serverError().entity("Parameters 'user', 'startDate' and 'endDate' must be provided!").build();
        }

		User currentUser = userDao.getUser(dataSource, user);
		if (currentUser == null || !currentUser.getActive()) {
			return Response.serverError().entity("The given user (" + user + ") not found or not active!").build();
		}

		List<Reservation> reservations = houseDao.getCalendar(dataSource, new Date(new SimpleDateFormat(datePattern).parse(startDate).getTime()), new Date(new SimpleDateFormat(datePattern).parse(endDate).getTime()));
        GenericEntity<List<Reservation>> entity = new GenericEntity<List<Reservation>>(reservations) {};
        return Response.ok(entity).build();
	}
 
	@GET
	@Path("/reservation/own")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Get own reservations",
		    responses = {
		    		@ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = Reservation.class))), responseCode = "200")
		    }
	)
	public Response getOwnReservations(
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client) throws SQLException {

        if (StringUtils.isBlank(client)) {
            return Response.serverError().entity("Parameter 'client' must be provided!").build();
        }

		User user = userDao.getUser(dataSource, client);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + client + ") not found or not active!").build();
		}

		List<Reservation> reservations = reservationDao.getReservations(dataSource, client);	
        GenericEntity<List<Reservation>> entity = new GenericEntity<List<Reservation>>(reservations) {};
        return Response.ok(entity).build();
	}

	@GET
	@Path("/reservation")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Get reservations",
		    responses = {
		    		@ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = Reservation.class))), responseCode = "200")
		    }
	)
	public Response getReservations(
			@Parameter(description = "Manager login name", required = true)
			@QueryParam("manager")
			String manager) throws SQLException {

        if (StringUtils.isBlank(manager)) {
            return Response.serverError().entity("Parameter 'manager' must be provided!").build();
        }

		User user = userDao.getUser(dataSource, manager);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + manager + ") not found or not active!").build();
		}
		if (!user.getRoles().contains(Role.MANAGER)) {
			return Response.serverError().entity("The given user does not have MANAGER role!").build();
		}

		List<Reservation> reservations = reservationDao.getReservations(dataSource, null);	
        GenericEntity<List<Reservation>> entity = new GenericEntity<List<Reservation>>(reservations) {};
        return Response.ok(entity).build();
	}

	@GET
	@Path("/task")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Get pending tasks",
		    responses = {
		    		@ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200")
		    }
	)
	public Response getTasks(
			@Parameter(description = "Manager or cleaner login name", required = true)
			@QueryParam("user")
			String user) throws SQLException {

        if (StringUtils.isBlank(user)) {
            return Response.serverError().entity("Parameter 'user' must be provided!").build();
        }

		User currentUser = userDao.getUser(dataSource, user);
		if (currentUser == null || !currentUser.getActive()) {
			return Response.serverError().entity("The given user (" + user + ") not found or not active!").build();
		}
		if (!currentUser.getRoles().contains(Role.MANAGER) && !currentUser.getRoles().contains(Role.CLEANER)) {
			return Response.serverError().entity("The given user does not have MANAGER or CLEANER role!").build();
		}

		House house = houseDao.getHouse(dataSource);
		String tasks = (house.getCleaned() ? "" : "Cleaning");
		if (!house.getPrepared()) {
			tasks = tasks + (StringUtils.isEmpty(tasks) ? "Preparing" : " and preparing");
		}	
        GenericEntity<String> entity = new GenericEntity<String>(StringUtils.isEmpty(tasks) ? "No task." : tasks) {};
        return Response.ok(entity).build();
	}

	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Get users",
		    responses = {
		    		@ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = User.class))), responseCode = "200")
		    }
	)
	public Response getUsers(
			@Parameter(description = "Manager login name", required = true)
			@QueryParam("manager")
			String manager) throws SQLException {

        if (StringUtils.isBlank(manager)) {
            return Response.serverError().entity("Parameter 'manager' must be provided!").build();
        }

		User user = userDao.getUser(dataSource, manager);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + manager + ") not found or not active!").build();
		}
		if (!user.getRoles().contains(Role.MANAGER)) {
			return Response.serverError().entity("The given user does not have MANAGER role!").build();
		}

		List<User> users = userDao.getUsers(dataSource);	
        GenericEntity<List<User>> entity = new GenericEntity<List<User>>(users) {};
        return Response.ok(entity).build();
	}

	@POST
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Add new client user",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response addUser(
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client,
			@Parameter(description = "Password", required = true)
			@QueryParam("password")
			String password,
			@Parameter(description = "Full name", required = true)
			@QueryParam("fullName")
			String fullName,
			@Parameter(description = "E-mail", required = true)
			@QueryParam("email")
			String email,
			@Parameter(description = "Phone number", required = false)
			@QueryParam("phoneNumber")
			String phoneNumber) throws SQLException {

        if (StringUtils.isAnyBlank(client, password, fullName, email)) {
            return Response.serverError().entity("Parameters 'client', 'password', 'fullName' and 'email' must be provided!").build();
        }

		if (userDao.getUser(dataSource, client) != null) {
			return Response.serverError().entity("The given client login name (" + client + ") already exists!").build();
		}

		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(seed);

		User user = new User();
		user.setLoginName(client);
		user.setPassword(encryptor.encrypt(password));
		user.setFullName(fullName);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		user.setActive(true);
		
		userDao.add(dataSource, user);
        return Response.ok().build();
	}
	
	@PUT
	@Path("/user/{user}/changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Change password",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response changePassword(			
			@Parameter(description = "User login name", required = true)
			@PathParam("user")
			String user,
			@Parameter(description = "Current password", required = true)
			@QueryParam("currentPassword")
			String currentPassword,
			@Parameter(description = "New password", required = true)
			@QueryParam("newPassword")
			String newPassword) throws SQLException {

        if (StringUtils.isAnyBlank(currentPassword, newPassword)) {
            return Response.serverError().entity("Parameters 'currentPassword' and 'newPassword' must be provided!").build();
        }

		if (StringUtils.equals(currentPassword, newPassword)) {
			return Response.serverError().entity("The given passwords are the same!").build();
		}

		User currentUser = userDao.getUser(dataSource, user);
		if (currentUser == null || !currentUser.getActive()) {
			return Response.serverError().entity("The given user (" + user + ") not found or not active!").build();
		}		

		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(seed);
		
		if (!StringUtils.equals(encryptor.decrypt(currentUser.getPassword()), currentPassword)) {
			return Response.serverError().entity("Invalid password given!").build();
		}

		userDao.changePassword(dataSource, user, encryptor.encrypt(newPassword));
        return Response.ok().build();
	}

	@GET
	@Path("/user/{user}/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Login",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response login(			
			@Parameter(description = "User login name", required = true)
			@PathParam("user")
			String user,
			@Parameter(description = "Password", required = true)
			@QueryParam("password")
			String password) throws SQLException {

        if (StringUtils.isBlank(password)) {
            return Response.serverError().entity("Parameter 'password' must be provided!").build();
        }

		User currentUser = userDao.getUser(dataSource, user);
		if (currentUser == null || !currentUser.getActive()) {
			return Response.serverError().entity("The given user (" + user + ") not found or not active!").build();
		}
		
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(seed);
		
		if (!StringUtils.equals(encryptor.decrypt(currentUser.getPassword()), password)) {
			return Response.serverError().entity("Invalid password given!").build();
		}

        return Response.ok().build();
	}

	@PUT
	@Path("/user/{client}/setStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Set client status",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response setStatus(			
			@Parameter(description = "Client login name", required = true)
			@PathParam("client")
			String client,
			@Parameter(description = "Active (TRUE/FALSE)", required = true)
			@QueryParam("active")
			String active) throws SQLException {

        if (StringUtils.isBlank(active)) {
            return Response.serverError().entity("Parameter 'active' must be provided!").build();
        }

		User user = userDao.getUser(dataSource, client);
		if (user == null) {
			return Response.serverError().entity("The given user (" + client + ") not found!").build();
		}
		if (user.getRoles().contains(Role.MANAGER) || user.getRoles().contains(Role.CLEANER)) {
			return Response.serverError().entity("Cannot set status for user with MANAGER OR CLEANER role!").build();
		}

		userDao.setStatus(dataSource, client, Boolean.parseBoolean(active));
        return Response.ok().build();
	}
	
	@POST
	@Path("/reservation")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Add reservation",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response addReservation(
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client,
            @Parameter(description = "Start date", required = true)
            @QueryParam("startDate")
			String startDate,
            @Parameter(description = "End date", required = true)
            @QueryParam("endDate")
			String endDate,
			@Parameter(description = "Number of persons", required = true)
			@QueryParam("numberOfPersons")
			String numberOfPersons,
			@Parameter(description = "Final cleaning (TRUE/FALSE)", required = true)
			@QueryParam("finalCleaning")
			String finalCleaning
			) throws ParseException, SQLException {
		
        if (StringUtils.isAnyBlank(client, startDate, endDate, numberOfPersons, finalCleaning)) {
            return Response.serverError().entity("Parameters 'client', 'startDate', 'endDate', 'numberOfPersons' and 'finalCleaning' must be provided!").build();
        }

		Reservation reservation = new Reservation();
		reservation.setSqlStartDate(new Date(new SimpleDateFormat(datePattern).parse(startDate).getTime()));
		reservation.setSqlEndDate(new Date(new SimpleDateFormat(datePattern).parse(endDate).getTime()));
		if (reservation.getSqlStartDate().after(reservation.getSqlEndDate()))  {
			return Response.serverError().entity("Start date is after the end date!").build();
		}
			
		User user = userDao.getUser(dataSource, client);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + client + ") not found or not active!").build();
		}

		House house = houseDao.getHouse(dataSource);
		if (house.getCapacity() < Integer.parseInt(numberOfPersons))  {
			return Response.serverError().entity("Not enough space!").build();
		}
		
		reservation.setClient(user);
		reservation.setNumberOfPersons(Integer.parseInt(numberOfPersons));
		reservation.setFinalCleaning(Boolean.parseBoolean(finalCleaning));
		
		boolean result = reservationDao.add(dataSource, reservation);
		if (!result)  {
			return Response.serverError().entity("The given time interval is already reserved!").build();
		}
			
        return Response.ok().build();
	}

	@PUT
	@Path("/reservation/{reservationId}/confirm")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Confirm reservation request by manager",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response confirmRequest(			
			@Parameter(description = "Reservation ID", required = true)
			@PathParam("reservationId")
			String reservationId,
			@Parameter(description = "Manager login name", required = true)
			@QueryParam("manager")
			String manager) throws SQLException {

        if (StringUtils.isBlank(manager)) {
            return Response.serverError().entity("Parameter 'manager' must be provided!").build();
        }
			
		User user = userDao.getUser(dataSource, manager);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + manager + ") not found or not active!").build();
		}
		if (!user.getRoles().contains(Role.MANAGER)) {
			return Response.serverError().entity("The given user does not have MANAGER role!").build();
		}
 
		boolean result = reservationDao.confirmRequest(dataSource, manager, Integer.parseInt(reservationId));
		if (!result)  {
			return Response.serverError().entity("The house is already reserved or reservation not found!").build();
		}
		
        return Response.ok().build();
	}
	
	@PUT
	@Path("/reservation/{reservationId}/deny")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Deny reservation request by manager",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response denyRequest(			
			@Parameter(description = "Reservation ID", required = true)
			@PathParam("reservationId")
			String reservationId,
			@Parameter(description = "Manager login name", required = true)
			@QueryParam("manager")
			String manager) throws SQLException {
		
        if (StringUtils.isBlank(manager)) {
            return Response.serverError().entity("Parameter 'manager' must be provided!").build();
        }
			
		User user = userDao.getUser(dataSource, manager);
		if (user == null || !user.getActive()) {
			return Response.serverError().entity("The given user (" + manager + ") not found or not active!").build();
		}
		if (!user.getRoles().contains(Role.MANAGER)) {
			return Response.serverError().entity("The given user does not have MANAGER role!").build();
		}

		boolean result = reservationDao.denyRequest(dataSource, manager, Integer.parseInt(reservationId));
		if (!result)  {
			return Response.serverError().entity("Reservation not found!").build();
		}

        return Response.ok().build();
	}
	
	@PUT
	@Path("/reservation/{reservationId}/cancel")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Cancel reservation",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response cancelRequest(			
			@Parameter(description = "Reservation ID", required = true)
			@PathParam("reservationId")
			String reservationId,
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client) throws SQLException {

        if (StringUtils.isBlank(client)) {
            return Response.serverError().entity("Parameter 'client' must be provided!").build();
        }
		
		boolean result = reservationDao.cancelRequest(dataSource, client, Integer.parseInt(reservationId));
		if (!result)  {
			return Response.serverError().entity("Only own reservation can be canceled or the reservation not found!").build();
		}

        return Response.ok().build();
	}

	@PUT
	@Path("/reservation/{reservationId}/checkIn")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			summary = "Check-in",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response checkIn(			
			@Parameter(description = "Reservation ID", required = true)
			@PathParam("reservationId")
			String reservationId,
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client) throws SQLException {

        if (StringUtils.isBlank(client)) {
            return Response.serverError().entity("Parameter 'client' must be provided!").build();
        }

		boolean result = reservationDao.checkIn(dataSource, client, Integer.parseInt(reservationId));
		if (!result)  {
			return Response.serverError().entity("Not own reservation or the reservation not found!").build();
		}
		
		houseDao.setReserved(dataSource, true);
		
        return Response.ok().build();
	}

	@PUT
	@Path("/reservation/{reservationId}/checkOut")
	@Produces(MediaType.APPLICATION_JSON) 
	@Operation(
			summary = "Check-out",
		    responses = {
		    		@ApiResponse(responseCode = "200")
		    }
	)
	public Response checkOut(			
			@Parameter(description = "Reservation ID", required = true)
			@PathParam("reservationId")
			String reservationId,
			@Parameter(description = "Client login name", required = true)
			@QueryParam("client")
			String client,
			@Parameter(description = "Cleaning status (TRUE/FALSE)", required = true)
			@QueryParam("cleaned")
			String cleaned,
			@Parameter(description = "Rating (integer)", required = false)
			@QueryParam("rating")
			String rating,
			@Parameter(description = "Client feedback", required = false)
			@QueryParam("feedback")
			String feedback) throws SQLException {

        if (StringUtils.isBlank(client)) {
            return Response.serverError().entity("Parameter 'client' must be provided!").build();
        }

		boolean result = reservationDao.checkOut(dataSource, client, Integer.parseInt(reservationId), rating == null ? 0 : Integer.parseInt(rating), feedback);
		if (!result)  {
			return Response.serverError().entity("Not own reservation or the reservation not found!").build();
		}

		houseDao.setReserved(dataSource, false);
		houseDao.setCleaned(dataSource, Boolean.parseBoolean(cleaned));
		houseDao.setPrepared(dataSource, false);
		
        return Response.ok().build();
	}

}
