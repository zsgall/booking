package com.housebooking.dao;

import com.housebooking.vo.Reservation;
import com.housebooking.vo.ReservationStatus;
import com.housebooking.vo.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;

public class ReservationDao {

	public boolean add(DataSource dataSource, Reservation reservation) throws ParseException, SQLException {

		final String insertReservationSql =
				"INSERT INTO reservation (id, start_date, end_date, client, number_of_persons, request_date, status, final_cleaning) " +
				"SELECT NEXTVAL('seq_reservation'), ?, ?, ?, ?, CURRENT_TIMESTAMP, 'PENDING', ? " +
				"FROM (SELECT 1) T " +
				"WHERE NOT EXISTS (SELECT 1 FROM reservation WHERE status = 'CONFIRMED' AND end_date >= ? AND start_date <= ?)";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(insertReservationSql);
        ) {        	
        	statement.setDate(1, reservation.getSqlStartDate());
            statement.setDate(2, reservation.getSqlEndDate());
            statement.setString(3, reservation.getClient().getLoginName());
            statement.setInt(4, reservation.getNumberOfPersons());
            statement.setBoolean(5, reservation.getFinalCleaning());
            statement.setDate(6, reservation.getSqlStartDate());
            statement.setDate(7, reservation.getSqlEndDate());
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}
	
	public boolean confirmRequest(DataSource dataSource, String loginName, int reservationId) throws SQLException {

		final String confirmRequestSql =
				"UPDATE reservation R1 SET status = 'CONFIRMED', responsed_by = ?, response_date = CURRENT_TIMESTAMP " +
				"WHERE id = ? AND NOT EXISTS " +
				"(SELECT 1 FROM reservation R2 WHERE R2.id <> R1.id AND R2.status = 'CONFIRMED' AND R2.end_date >= R1.start_date AND R2.start_date <= R1.end_date)";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(confirmRequestSql)
        ) {
        	statement.setString(1, loginName);
            statement.setInt(2, reservationId);
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}

	public boolean denyRequest(DataSource dataSource, String loginName, int reservationId) throws SQLException {

		final String denyRequestSql =
				"UPDATE reservation SET status = 'DENIED', responsed_by = ?, response_date = CURRENT_TIMESTAMP " +
				"WHERE id = ? ";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(denyRequestSql)
        ) {
        	statement.setString(1, loginName);
            statement.setInt(2, reservationId);
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}

	public boolean cancelRequest(DataSource dataSource, String loginName, int reservationId) throws SQLException {
		final String cancelRequestSql =
				"DELETE FROM reservation WHERE id = ? AND client = ?";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(cancelRequestSql)
        ) {
        	statement.setInt(1, reservationId);
            statement.setString(2, loginName);
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}

	public boolean checkIn(DataSource dataSource, String loginName, int reservationId) throws SQLException {

		final String checkInSql =
				"UPDATE reservation SET check_in_date = CURRENT_TIMESTAMP " +
				"WHERE id = ? AND client = ?";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(checkInSql);
        ) {
            statement.setInt(1, reservationId);
        	statement.setString(2, loginName);
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}

	public boolean checkOut(DataSource dataSource, String loginName, int reservationId, int rating, String feedback) throws SQLException {

		final String checkOutSql =
				"UPDATE reservation SET check_out_date = CURRENT_TIMESTAMP, rating = ?, feedback = ? " +
				"WHERE id = ? AND client = ?";

		int affectedRows = 0;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(checkOutSql);
        ) {
        	statement.setInt(1, rating);
        	statement.setString(2, feedback);
            statement.setInt(3, reservationId);
        	statement.setString(4, loginName);
            affectedRows = statement.executeUpdate();
        }
        return affectedRows > 0;
	}
	
	public List<Reservation> getReservations(DataSource dataSource, String loginName) throws SQLException {

		final String getReservationsSql =
				"SELECT id, start_date, end_date, client, number_of_persons, request_date, status, responsed_by, response_date, " +
				"final_cleaning, rating, feedback, check_in_date, check_out_date " +
				"FROM reservation WHERE 1 = 1" + (StringUtils.isEmpty(loginName) ? "" : " AND client = ?");
		
		UserDao userDao = new UserDao();
		Map<String, User> userMap = userDao.getUserMap(dataSource);		
		List<Reservation> reservations = new ArrayList<>();
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(getReservationsSql)
        ) {
    		if (StringUtils.isNotEmpty(loginName)) {
    			statement.setString(1, loginName);
    		}
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Reservation reservation = new Reservation();
                reservation.setId(resultSet.getInt("id"));
                reservation.setSqlStartDate(resultSet.getDate("start_date"));
                reservation.setSqlEndDate(resultSet.getDate("end_date"));
                reservation.setClient(userMap.get(resultSet.getString("client")));
                reservation.setNumberOfPersons(resultSet.getInt("number_of_persons"));
                reservation.setSqlRequestDate(resultSet.getTimestamp("request_date"));
                reservation.setStatus(ReservationStatus.valueOf(resultSet.getString("status")));
                reservation.setResponsedBy(userMap.get(resultSet.getString("responsed_by")));
                reservation.setSqlResponseDate(resultSet.getTimestamp("response_date"));
                reservation.setFinalCleaning(resultSet.getBoolean("final_cleaning"));
                reservation.setRating(resultSet.getInt("rating"));
                reservation.setFeedback(resultSet.getString("feedback"));
                reservation.setSqlCheckInDate(resultSet.getTimestamp("check_in_date"));
                reservation.setSqlCheckOutDate(resultSet.getTimestamp("check_out_date"));
                reservations.add(reservation);
            }
        }
		
        return reservations;
	}
}
