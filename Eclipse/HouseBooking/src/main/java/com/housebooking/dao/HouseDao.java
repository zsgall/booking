package com.housebooking.dao;

import com.housebooking.vo.House;
import com.housebooking.vo.Reservation;
import com.housebooking.vo.ReservationStatus;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class HouseDao {

	public House getHouse(DataSource dataSource) throws SQLException {
		 
		final String getHouseSql =
				"SELECT capacity, address, cleaned, prepared, reserved FROM house LIMIT 1";
		
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(getHouseSql);
        ) {
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
            	throw new SQLException("House record missing!");
            }	
            House house = new House();
            house.setCapacity(resultSet.getInt("capacity"));
            house.setAddress(resultSet.getString("address"));
            house.setCleaned(resultSet.getBoolean("cleaned"));
            house.setPrepared(resultSet.getBoolean("prepared"));
            house.setReserved(resultSet.getBoolean("reserved"));
            return house;
        }
	}
	
	public void setCleaned(DataSource dataSource, boolean cleaned) throws SQLException {
		
		final String setCleanedSql =
				"UPDATE house SET cleaned = ?";
		
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(setCleanedSql);
        ) {
            statement.setBoolean(1, cleaned);
            statement.executeUpdate();            
        }
	}

	public void setPrepared(DataSource dataSource, boolean prepared) throws SQLException {
		
		final String setPreparedSql =
				"UPDATE house SET prepared = ?";
		
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(setPreparedSql);
        ) {
            statement.setBoolean(1, prepared);
            statement.executeUpdate();            
        }
	}

	public void setReserved(DataSource dataSource, boolean reserved) throws SQLException {
		
		final String setReservedSql =
				"UPDATE house SET reserved = ?";
		
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(setReservedSql);
        ) {
            statement.setBoolean(1, reserved);
            statement.executeUpdate();            
        }
	}
	
	public List<Reservation> getCalendar(DataSource dataSource, Date startDate, Date endDate) throws SQLException {

		final String getCalendarSql =
				"SELECT id, start_date, end_date, final_cleaning, number_of_persons " +
				"FROM reservation WHERE status = 'CONFIRMED' AND ? <= end_date AND ? >= start_date";
		
		List<Reservation> reservations = new ArrayList<>();
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(getCalendarSql)
        ) {
    		statement.setDate(1, startDate);
    		statement.setDate(2, endDate);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Reservation reservation = new Reservation();
                reservation.setId(resultSet.getInt("id"));
                reservation.setSqlStartDate(resultSet.getDate("start_date"));
                reservation.setSqlEndDate(resultSet.getDate("end_date"));
                reservation.setStatus(ReservationStatus.CONFIRMED);
                reservation.setFinalCleaning(resultSet.getBoolean("final_cleaning"));
                reservation.setNumberOfPersons(resultSet.getInt("number_of_persons"));
                reservations.add(reservation);
            }
        }
		
        return reservations;		
	}

}
