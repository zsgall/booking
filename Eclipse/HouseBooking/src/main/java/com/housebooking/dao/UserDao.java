package com.housebooking.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;

import com.housebooking.vo.Role;
import com.housebooking.vo.User;

public class UserDao {

	public User getUser(DataSource dataSource, String loginName) throws SQLException {
 
		final String getUserSql =
				"SELECT active, email, full_name, password, phone_number, " +
		        "(SELECT STRING_AGG(role_name, ',' ORDER BY role_name) FROM user_role UR WHERE UR.login_name = U.login_name) AS roles " +
		        "FROM app_user U WHERE login_name = ?";
		
		User user = null;
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(getUserSql);
        ) {
            statement.setString(1, loginName);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setActive(resultSet.getBoolean("active"));
                user.setEmail(resultSet.getString("email"));
                user.setFullName(resultSet.getString("full_name"));
                user.setLoginName(loginName);
                user.setPassword(resultSet.getString("password"));
                user.setPhoneNumber(resultSet.getString("phone_number"));
                String roles = resultSet.getString("roles");
                for (String role : StringUtils.split(roles, ",")) {
                	user.addRole(Role.valueOf(role));
                }
            }
        }
        
        return user;
	}
	
	public void add(DataSource dataSource, User user) throws SQLException {
		
		final String insertUserSql =
				"INSERT INTO app_user (login_name, password, full_name, email, phone_number, active) " +
				"VALUES (?, ?, ?, ?, ?, ?)";

		final String insertRoleSql =
				"INSERT INTO user_role (login_name, role_name) " +
				"VALUES (?, ?)";

        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement1 = connection.prepareStatement(insertUserSql);
        		PreparedStatement statement2 = connection.prepareStatement(insertRoleSql)
        ) {
        	statement1.setString(1, user.getLoginName());
            statement1.setString(2, user.getPassword());
            statement1.setString(3, user.getFullName());
            statement1.setString(4, user.getEmail());
            statement1.setString(5, user.getPhoneNumber());
            statement1.setBoolean(6, true);
            statement1.executeUpdate();
            
        	statement2.setString(1, user.getLoginName());
            statement2.setString(2, "CLIENT");
            statement2.executeUpdate();
        }
	}
	
	public void changePassword(DataSource dataSource, String loginName, String password) throws SQLException {

		final String changePasswordSql = "UPDATE app_user SET password = ? WHERE login_name = ?";

        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(changePasswordSql)
        ) {
        	statement.setString(1, password);
            statement.setString(2, loginName);
            statement.executeUpdate();            
        }
	}
	
	public void setStatus(DataSource dataSource, String loginName, boolean active) throws SQLException {

		final String setStatusSql = "UPDATE app_user SET active = ? WHERE login_name = ?";

        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(setStatusSql)
        ) {
        	statement.setBoolean(1, active);
            statement.setString(2, loginName);
            statement.executeUpdate();            
        }
	}

	public List<User> getUsers(DataSource dataSource) throws SQLException {
			
		final String getUsersSql =
				"SELECT active, email, full_name, login_name, phone_number, " +
				"(SELECT STRING_AGG(role_name, ',' ORDER BY role_name) FROM user_role UR WHERE UR.login_name = U.login_name) AS roles " +
				"FROM app_user U";
		
		List<User> users = new ArrayList<>();
        try (
        		Connection connection = dataSource.getConnection();
        		PreparedStatement statement = connection.prepareStatement(getUsersSql);
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setActive(resultSet.getBoolean("active"));
                user.setEmail(resultSet.getString("email"));
                user.setFullName(resultSet.getString("full_name"));
                user.setLoginName(resultSet.getString("login_name"));
                user.setPhoneNumber(resultSet.getString("phone_number"));
                String roles = resultSet.getString("roles");
                for (String role : StringUtils.split(roles, ",")) {
                	user.addRole(Role.valueOf(role));
                }
                users.add(user);
            }
        }
		
        return users;
	}
	
	public Map<String, User> getUserMap(DataSource dataSource) throws SQLException {
		List<User> users = getUsers(dataSource);
		Map<String, User> userMap = new HashMap<>();
		for (User user : users) {
			userMap.put(user.getLoginName(), user);
		}
		return userMap;
	}
}
