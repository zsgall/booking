CREATE TABLE house (
    capacity INTEGER NOT NULL,
    address  VARCHAR(255) NOT NULL,
    cleaned  BOOLEAN NOT NULL,
    prepared BOOLEAN NOT NULL,
	reserved BOOLEAN NOT NULL
);

INSERT INTO house (capacity, address, cleaned, prepared, reserved)
VALUES (10, 'Test street 34.', TRUE, TRUE, FALSE);

CREATE TABLE app_user (
    login_name   VARCHAR(50) NOT NULL,
    password     VARCHAR(50) NOT NULL,
    full_name    VARCHAR(255) NOT NULL,
    email        VARCHAR(255) NOT NULL,
    phone_number VARCHAR(50),
    active       BOOLEAN NOT NULL,
    CONSTRAINT pk_app_user PRIMARY KEY (login_name)
);

INSERT INTO app_user (login_name, password, full_name, email, active)
VALUES ('manager', 'kPcMb0V+vdSCyFJm79yACQ==', 'Manager', 'manager@testhost.com', TRUE);

INSERT INTO app_user (login_name, password, full_name, email, active)
VALUES ('cleaner', 'jBX1rsZ8Op59PtwBQqQp0A==', 'Cleaner', 'cleaner@testhost.com', TRUE);

CREATE TABLE user_role (
    login_name VARCHAR(50) NOT NULL,
    role_name  VARCHAR(50) NOT NULL,
    CONSTRAINT pk_user_role PRIMARY KEY (login_name, role_name)
);

INSERT INTO user_role (login_name, role_name)
VALUES ('manager', 'MANAGER');

INSERT INTO user_role (login_name, role_name)
VALUES ('manager', 'CLIENT');

INSERT INTO user_role (login_name, role_name)
VALUES ('cleaner', 'CLEANER');

CREATE SEQUENCE seq_reservation START WITH 1 INCREMENT BY 1;

CREATE TABLE reservation (
	id                INTEGER NOT NULL,
    start_date        DATE NOT NULL,
    end_date          DATE NOT NULL,
    client            VARCHAR(50) NOT NULL,
    number_of_persons INTEGER NOT NULL,
    request_date      TIMESTAMP NOT NULL,
    status            VARCHAR(50) NOT NULL,
    responsed_by      VARCHAR(50),
    response_date     TIMESTAMP,
    final_cleaning    BOOLEAN NOT NULL,
    rating            INTEGER,
    feedback          VARCHAR(1000),
    check_in_date     TIMESTAMP,
    check_out_date    TIMESTAMP,
    CONSTRAINT pk_reservation PRIMARY KEY (id)
);

COMMIT;