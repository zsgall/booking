import { Component, OnInit } from '@angular/core';

import { AuthService } from './../../../_auth/services/auth.service';
import {User} from '../../../generated';
import RolesEnum = User.RolesEnum;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  navToggle: Boolean = false;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

  toggleNav() {
    this.navToggle  = !this.navToggle;
  }

  cleanerRight() {
    return this.authService.userHasRole(RolesEnum.CLEANER);
  }

  managerRight() {
    return this.authService.userHasRole(RolesEnum.MANAGER);
  }
}
