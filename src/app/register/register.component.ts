import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DefaultService} from '../generated';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loginForm: FormGroup;
  processing: Boolean = false;
  error: Boolean = false;

  constructor(
    private defaultService: DefaultService,
    private router: Router
  ) {
    this.initForm();
  }

  ngOnInit(): void {
  }

  private initForm() {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      fullName: new FormControl('', Validators.required),
      email: new FormControl('', [ Validators.required, Validators.email]),
      phoneNumber: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  onSubmitButtonClicked() {
    this.error  = false;
    this.processing  = false;
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      this.defaultService.addUser(
        this.loginForm.get('username').value,
        this.loginForm.get('password').value,
        this.loginForm.get('fullName').value,
//        this.loginForm.get('email').value,
        'xxxxx',
        this.loginForm.get('phoneNumber').value
      ).subscribe((value) => {
        this.router.navigate(['/login']);
      });
    }
  }
}
