import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MngresComponent } from './mngres.component';

describe('MngresComponent', () => {
  let component: MngresComponent;
  let fixture: ComponentFixture<MngresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MngresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MngresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
