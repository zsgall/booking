import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {DefaultService, Reservation} from '../generated';
import {TableData} from '../newres/newres.component';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-mngres',
  templateUrl: './mngres.component.html',
  styleUrls: ['./mngres.component.scss']
})
export class MngresComponent implements OnInit {

  dataSource = new MatTableDataSource<Reservation>([]);
  displayedColumns: string[] = ['startDate', 'endDate', 'numberOfPersons', 'clientEmail', 'clientPhone', 'status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private defaultService: DefaultService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {
    this.defaultService.getReservations('manager').subscribe(res => {
      this.dataSource = new MatTableDataSource<Reservation>(res);
      this.dataSource.paginator = this.paginator;
    });
  }

  accept(id) {
    console.log('accept', id);
    this.defaultService.confirmRequest(id, 'manager').subscribe((res) => {
      this.loadData();
    });
  }

  deny(id) {
    console.log('deny', id);
    this.defaultService.cancelRequest(id, 'manager').subscribe((res) => {
      this.loadData();
    });
  }
}
