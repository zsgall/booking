import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { routingModule } from './app.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { UiModule } from './_shared/ui/ui.module';
import { FakeBackendInterceptor } from './_shared/fakebackend';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_auth/guards/auth.guard';
import { TokenIntercept } from './_auth/tokenintercept';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

import { ItemsModule } from './items/items.module';

import { ItemsService } from './items/_services/items.service';
import { AuthService } from './_auth/services/auth.service';
import {ApiModule} from './generated';

import {BASE_PATH} from './generated';
import { RegisterComponent } from './register/register.component';
import { NewresComponent } from './newres/newres.component';
import {AngularMaterialModule} from './material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule} from '@angular/material/core';
import {DatePipe} from '@angular/common';
import { ClnresComponent } from './clnres/clnres.component';
import { MngresComponent } from './mngres/mngres.component';
import { OwnresComponent } from './ownres/ownres.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    LogoutComponent,
    RegisterComponent,
    NewresComponent,
    ClnresComponent,
    MngresComponent,
    OwnresComponent,
  ],
  imports: [
    BrowserModule,
    UiModule,
    HttpClientModule,
    RouterModule,
    routingModule,
    ReactiveFormsModule,
    FormsModule,
    ItemsModule,
    ApiModule,
    AngularMaterialModule,
    BrowserAnimationsModule,
    MatNativeDateModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/'},
    AuthGuard,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenIntercept,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FakeBackendInterceptor,
      multi: true
    },
    {
      provide: BASE_PATH,
      useFactory: getBaseLocation
    },
    AuthService,
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseLocation() {
  return ' ';
}
