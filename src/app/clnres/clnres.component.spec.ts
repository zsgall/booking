import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClnresComponent } from './clnres.component';

describe('ClnresComponent', () => {
  let component: ClnresComponent;
  let fixture: ComponentFixture<ClnresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClnresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClnresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
