import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {DefaultService, Reservation} from '../generated';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-clnres',
  templateUrl: './clnres.component.html',
  styleUrls: ['./clnres.component.scss']
})
export class ClnresComponent implements OnInit {

  dataSource = new MatTableDataSource<Reservation>([]);
  displayedColumns: string[] = ['startDate', 'endDate', 'numberOfPersons', 'status', 'finalCleaning'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private defaultService: DefaultService
  ) { }

  ngOnInit(): void {
    this.defaultService.getReservations('manager').subscribe(res => {
      this.dataSource = new MatTableDataSource<Reservation>(res);
      this.dataSource.paginator = this.paginator;
    })
  }

}
