import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {DefaultService, Reservation} from '../generated';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {DatePipe} from '@angular/common';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';


export interface TableData {
  date: Date;
  status: string;
}

@Component({
  selector: 'app-newres',
  templateUrl: './newres.component.html',
  styleUrls: ['./newres.component.scss']
})
export class NewresComponent implements OnInit, AfterViewInit {

  myForm: FormGroup;
  resForm: FormGroup;
  checked: any;
  dataSource = new MatTableDataSource<TableData>([]);

  reservations: Reservation[] = [];

  displayedColumns: string[] = ['date', 'status'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private fb: FormBuilder,
    private defaultService: DefaultService,
    private datePipe: DatePipe,
    private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      sdate: [''],
      edate: ['']
    });

    this.resForm = this.fb.group({
      sdate: [''],
      edate: [''],
      guestNumb: [0],
      cleaning: [0]
    });
  }

  filter() {
    const ds: TableData[] = [];

    let fromDate = this.myForm.get('sdate').value;
    let toDate = this.myForm.get('edate').value;

    console.log(fromDate);
    console.log(toDate);

    this.defaultService.getReservations('manager').subscribe(res => {
      this.reservations = res;

      while (fromDate <= toDate) {
        ds.push({date: new Date(fromDate), status: 'Available'});
        fromDate.setDate( fromDate.getDate() + 1);
      }

      for (let e of ds) {
        for (let r of res) {
          if (e.date >= new Date(r.startDate) && e.date <= new Date(r.endDate) ) {
            e.status = 'Reserved';
          }
        }
      }

      this.dataSource = new MatTableDataSource<TableData>(ds);
      this.dataSource.paginator = this.paginator;
    });

  }

  submitForm() {

  }

  submitResForm() {

  }

  reserve() {
    let fromDate = this.datePipe.transform(this.resForm.get('sdate').value,"yyyy-MM-dd");
    let toDate = this.datePipe.transform(this.resForm.get('edate').value,"yyyy-MM-dd");
    let guestNumb = this.resForm.get('guestNumb').value;
    let cleaning = this.resForm.get('cleaning').value;

    console.log(fromDate, toDate, guestNumb, cleaning);

    this.defaultService
      .addReservation('manager', fromDate, toDate, guestNumb, cleaning)
      .subscribe((res) => {
        this._snackBar.open('Reservation has been registered.', 'OK', {
          horizontalPosition: 'end',
          verticalPosition: 'top',
          panelClass: 'my-custom-snackbar'
        });

        this.resForm.reset();
        this.myForm.reset();

        this.dataSource = new MatTableDataSource<TableData>([]);
        this.dataSource.paginator = this.paginator;
      },
        (error) => {
          this._snackBar.open('Reservation cancelled!', 'OK', {
            horizontalPosition: 'end',
            verticalPosition: 'top',
            panelClass: 'my-custom-snackbar'
          });
        });
  }
}
