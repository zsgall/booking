import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnresComponent } from './ownres.component';

describe('OwnresComponent', () => {
  let component: OwnresComponent;
  let fixture: ComponentFixture<OwnresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
