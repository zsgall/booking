import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {DefaultService, Reservation} from '../generated';
import {MatPaginator} from '@angular/material/paginator';
import {AuthService} from '../_auth/services/auth.service';

@Component({
  selector: 'app-ownres',
  templateUrl: './ownres.component.html',
  styleUrls: ['./ownres.component.scss']
})
export class OwnresComponent implements OnInit {

  dataSource = new MatTableDataSource<Reservation>([]);
  displayedColumns: string[] = ['startDate', 'endDate', 'numberOfPersons', 'status', 'finalCleaning', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private defaultService: DefaultService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  call_checkin(id) {
    this.defaultService.checkIn(id, this.authService.getUser().loginName).subscribe(res => {
      this.loadData();
    });
  }

  call_checkout(id) {
    this.defaultService.checkOut(id, this.authService.getUser().loginName, 'true').subscribe(res => {
      this.loadData();
    });
  }

  loadData() {
    this.defaultService.getOwnReservations(this.authService.getUser().loginName).subscribe(res => {
      this.dataSource = new MatTableDataSource<Reservation>(res);
      this.dataSource.paginator = this.paginator;
      console.log('data loaded2');

    });
  }

}
